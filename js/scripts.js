// When the document has finished loading
// (necessary since the event handlers below will not work unless the selected elements exist on the page)
$(document).ready(function() 
{
  // Shows or hides the submenu that was clicked on
  $(".dropdown-submenu").click(function(event)
  {
    const toToggle = $(this).children().children(".dropdown-menu").first();
    
    console.log(`Should show the menu: ${$(toToggle).is(":hidden")}`);

    // Hide all submenus that are not in the path to this menu item
    $(".dropdown-submenu .dropdown-menu").filter(function() 
    {
      console.log($(this));
      console.log($(toToggle));
      console.log(`  ${$(this).is($(toToggle))}`);
      console.log(`  ${$(toToggle).parents().toArray().includes(this)}`);
      return !(($(this).is($(toToggle))) || $(toToggle).parents().toArray().includes(this));
    }).hide();
    
    // Toggle the menu item itself
    console.log(`Should show the menu: ${$(toToggle).is(":hidden")}`);
    $(toToggle).toggle();
    event.stopPropagation();
  });
  
  // Hides submenus when any other menu item is clicked upon
  $(".dropdown-submenu .dropdown-item:not(.dropdown-submenu)").click(function() 
  {
    $(".dropdown-submenu .dropdown-menu").hide();
    $(".dropdown-menu").removeClass("show");
    event.stopPropagation();
  });
});